//g++ -std=c++0x serialcommander.cpp -o serialcommander
#pragma GCC diagnostic ignored "-Wwrite-strings" //To ignore string conversion warnings

#include <iostream>  //for prints
#include <stdio.h>   //for sprintf
#include <string.h>  // for strlen
#include <string>
#include <fstream>   // for ifstream, ofstream
#include <stdlib.h>  // for system
#include <sstream>
#include <unistd.h>
#include <cstdlib>

using std::cout;
using std::cin;
using std::string;
using std::ifstream;
using std::ostringstream;

//Data
string command;

string serial = "/dev/ttyS0";
bool debug = false;

bool end = false;

//Function Initialisation
void set(string command);

int main(int argc, char* args[]){
								while (command != "_end_" || end != true) {

																getline(cin,command);
																set(command);

																if (command == "") {
																								end = true;
																}
								}
}

//Print data to serial in correct format
void set(string command)
{
									ostringstream data;
									data << "echo '" << '^' << command << "'";
									system(data.str().c_str());
}
