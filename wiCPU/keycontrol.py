#!/usr/bin/env python2
import getkey
import os


print "Use  [w-forward, s-backward, a-left, d-right] to control the robot and [q-start, e-end], press esc to exit."

# chars = ["q", "a", "w", "d", "s", "e"]

start = "q"
end = "e"

left = "a"
right = "d"
forward = "w"
backward = "s"


esckey = False

key = getkey.getch()
while esckey == False:
    key = getkey.getch()

    if key == start:
        os.system("python master.py S")
        print "Start"

    if key == left:
        os.system("python master.py L0 R90")
        print "Left"

    if key == right:
        os.system("python master.py L90 R0")
        print "Right"

    if key == forward:
        os.system("python master.py L0 R0")
        print "Forward"

    if key == backward:
        os.system("python master.py L180 R180")
        print "Backward"

    if key == end:
        esckey = True
        os.system("python master.py E")
        print "Exit"

print "Program ended..."
