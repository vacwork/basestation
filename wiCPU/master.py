#!/usr/bin/env python2

import subprocess
import sys

proc = subprocess.Popen("./serialcommander",
stdin=subprocess.PIPE,
stdout=subprocess.PIPE)

if len(sys.argv) >= 2:
    process = False
    for arg in sys.argv:
            if process == True:
                input = arg
                proc.stdin.write(input + "\n")
                print proc.stdout.readline().rstrip("\n")
            process = True
proc.terminate()
