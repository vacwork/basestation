#pragma GCC diagnostic ignored "-Wwrite-strings" //To ignore string conversion warnings

#include <cstdlib>
#include <iostream>  //for prints
#include <stdio.h>   //for sprintf
#include <string.h>  // for strlen
#include <string>
#include <fstream>   // for ifstream, ofstream
#include <stdlib.h>  // for system
#include <sstream>
#include <unistd.h>

using std::ifstream;
using std::string;

//Wifi
string wifiname,wifipass;

bool debug = true;

void wifi(void);
void clearout(void);
string exec(char* cmd);

int main(int argc, char const *argv[]) {
        system("./blink 0 0 1 >/dev/null");
        wifi();
        clearout();
        if (debug==true) {
                printf("%s\n", "Initialisation Complete!");
        }
        return 0;
}

//Connect to wifi (setup in config.txt)
void wifi(){

        ifstream savefile ("config.txt");

        if(savefile.is_open())
        {
                getline(savefile,wifiname);
                getline(savefile,wifipass);
                savefile.close();
        }

        else
        {
                wifiname="";
                wifipass="";
                if (debug==true) {
                        printf("%s\n", "WiFi: No config file!");
                        system("./blink 1 0 0 >/dev/null");
                }
        }

        //Use Config settings to connect to wifi
        string statW_up = exec("ip link | grep wlan");

        if(!(statW_up.find("state UP") != string::npos))
        {
                if (debug==true) {
                        printf("%s\n", "WiFi: Connecting...");
                }
                system(("/home/wiApp/base/wifi.sh \""+wifiname+"\" \""+wifipass+"\"").c_str());
                system("./blink 0 1 0 >/dev/null");
        }
        else{
          system("./blink 0 1 0 >/dev/null");
        }
}

//Clear RAM and kill startup programs
void clearout()
{
        if (debug==true) {
                printf("%s\n", "Clearing RAM");
        }
        system("bash /home/wiApp/base/clearout.sh");
}

//Extract data from file
string exec(char* cmd)
{
        FILE* pipe = popen(cmd, "r");
        if (!pipe) return "ERROR";
        char buffer[128];
        std::string result = "";
        while(!feof(pipe))
        {
                if(fgets(buffer, 128, pipe) != NULL)
                        result += buffer;
        }
        pclose(pipe);
        return result;
}
