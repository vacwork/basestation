#!/bin/sh
killall simplemenu 2&>1 >/dev/null
killall hibernatemenu 2&>1 >/dev/null

rmmod sun4i_csi0
rmmod videobuf_dma_contig
rmmod videobuf_core
rmmod ov2643

free -h > /dev/null
sudo echo 1 > /proc/sys/vm/drop_caches
sudo echo 2 > /proc/sys/vm/drop_caches
sudo echo 3 > /proc/sys/vm/drop_caches
free -h > /dev/null

modprobe sun4i_csi0
