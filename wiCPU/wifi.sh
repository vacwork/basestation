wlan=`ifconfig -a | grep -o "^wlan[0-9]\+"`
f="/etc/network/interfaces"
echo $wlan
echo "auto lo" > $f
echo "iface lo inet loopback" >> $f
echo "" >> $f
echo "auto $wlan" >> $f
echo "iface $wlan inet dhcp" >> $f
echo "wpa-ssid \"$1\"" >> $f
echo "wpa-psk \"$2\"" >> $f
ifdown $wlan
service network-manager restart
ifup $wlan
